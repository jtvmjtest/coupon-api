# coupon-api

API realizada para la prueba técnica para desarrollador backend en MrJeff.

## Apartados

### Implementación escenario 1

**Endpoint:**  /coupon/discount

##### Ejemplo body request:

```json
{
"couponCode": "CUPON-B",
	"products": [{"productCode": "PANTALON", "price": 10}]
}
```
##### Ejemplo body response:
```json
{
  "totalPrice": 10.0,
  "discountedPrice": 0.0,
  "products": [
    {
      "productCode": "PANTALON",
      "price": 10.0
    }
  ]
}
```

### Implementación escenario 2
**Endpoint:**  /coupon/search/{status}/{couponType}

* status: active o inactive
* couponType: tipo del cupón a buscar

##### Ejemplo endpoint request:

```
http://localhost:8080/coupon/search/active/GRATIS
```
##### Ejemplo body response:
```json
{
  "coupons": [
    {
      "couponCode": "CUPON-A",
      "couponType": {
        "couponType": "GRATIS",
        "discountAmount": null,
        "discountPercentage": 100.0,
        "description": "El descuento es igual al total"
      }
    },
    {
      "couponCode": "CUPON-B",
      "couponType": {
        "couponType": "GRATIS",
        "discountAmount": null,
        "discountPercentage": 100.0,
        "description": "El descuento es igual al total"
      }
    }
  ]
}
```


### Gestión de excepciones y errores en el API

Se han desarrollado dos clases de Exception para los casos en los que el API devuelve un error según las especificaciones:

* **CouponDisabledException:** Cuando se intenta aplicar un código inactivo
* **InvalidSearchRequestException:** Cuando se intenta buscar un cupón introduciendo un {status} incorrecto

En ambos casos se devuelve un código 500 BAD REQUEST

### Tests unitarios y de integración

* Se han desarrollado tests unitarios de controlador y servicio usando Mockito. Los tests han sido basados en los ejemplos de consultas en las especificaciones.
* El test de integración crea una base de datos H2 en memoria con los mismos datos que la MySQL de producción y se prueba la aplicación entera usando el WebTestClient

## Deployment

Se trata de una aplicación Spring Boot 2 estándar. La estructura y rellenado de la base de datos se hacen al arrancar la aplicación: simplemente hay que configurar una base de datos y arrancar el jar.

Es necesaria una base de datos. Por defecto, se ha configurado con los siguientes parámetros para una base de datos MySQL:

```
spring.datasource.url=jdbc:mysql://localhost/coupon
spring.datasource.username=coupon
spring.datasource.password=coupon
```

Para el desarrollo se ha usado el container oficial de MySQL en Docker. Se ha probado con la última versión, MySQL 8

```
sudo docker run --name coupon-api-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=rootpassword -d mysql:8
```

Para crear usuario y base de datos, ejecutar con el usuario root:

```
CREATE USER 'coupon'@'%' IDENTIFIED BY 'coupon';
GRANT ALL PRIVILEGES ON * . * TO 'coupon'@'%';
flush privileges;
```

Y con el usuario recién creado:

```
create database coupon
```

