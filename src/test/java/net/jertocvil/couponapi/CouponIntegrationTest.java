package net.jertocvil.couponapi;

import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.repository.CouponTypeRepository;
import net.jertocvil.couponapi.service.PriceService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class CouponIntegrationTest {

    private Logger LOG = LoggerFactory.getLogger(CouponIntegrationTest.class);

    @Autowired
    private WebTestClient webClient;

    @Autowired
    CouponTypeRepository couponTypeRepository;

    @Autowired
    CouponRepository couponRepository;

    private void priceAssert(String coupon, double price) throws Exception {
        priceAssert(coupon, price, null);
    }

    private void priceAssert(String coupon, double price, JSONObject[] products) throws JSONException {

        JSONArray jsonProducts;

        if(products == null) {
            JSONObject camisa = new JSONObject();
            camisa.put("productCode", "CAMISA");
            camisa.put("price", 3.0);

            JSONObject pantalon = new JSONObject();
            pantalon.put("productCode", "TRAJE");
            pantalon.put("price", 12.0);

            jsonProducts = new JSONArray(Arrays.asList(camisa, pantalon));
        } else {
            jsonProducts = new JSONArray(products);
        }



        JSONObject requestData = new JSONObject();
        requestData.put("couponCode", coupon);
        requestData.put("products", jsonProducts);

        webClient.method(HttpMethod.GET).uri("/coupon/discount").contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8).body(Mono.just(requestData.toString()), String.class)
                .exchange().expectBody().jsonPath("$.discountedPrice").value(is(price), Double.class);

    }

    // Test discount

    @Test
    public void withoutCoupon() throws Exception {

        this.priceAssert("NOEXISTE", 15.0);
    }

    @Test
    public void freeCoupon() throws Exception {
        this.priceAssert("CUPON-A", 0.0);
    }

    @Test
    public void withAmountCoupon() throws Exception {
        this.priceAssert("CUPON-D", 5.0);
    }

    @Test
    public void withPercentageCoupon() throws Exception {
        this.priceAssert("CUPON-E", 14.25);
    }

    @Test
    public void withCouponAmountGreaterThanPrice() throws Exception {
        JSONObject camisa = new JSONObject();
        camisa.put("productCode", "CAMISA");
        camisa.put("price", 3.0);

        this.priceAssert("CUPON-D", 0.0, new JSONObject[]{camisa});

    }

    @Test
    public void inactiveCoupon() throws Exception {
        JSONObject requestData = new JSONObject();
        requestData.put("couponCode", "CUPON-G");
        requestData.put("products", new JSONArray());

        webClient.method(HttpMethod.GET).uri("/coupon/discount").contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8).body(Mono.just(requestData.toString()), String.class)
                .exchange().expectStatus().isBadRequest();

    }


    // Tests coupon search

    @Test
    public void emptyCouponSearch() {
        webClient.method(HttpMethod.GET)
                .uri("/coupon/search/{status}/{couponType}", PriceService.STATUS_INACTIVE, "FIJO-5")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange().expectBody().jsonPath("$.coupons").isEmpty();
    }

    @Test
    public void couponSearch() {
        webClient.method(HttpMethod.GET)
                .uri("/coupon/search/{status}/{couponType}", PriceService.STATUS_ACTIVE, "GRATIS")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange().expectBody().jsonPath("$.coupons[*].couponCode",
                containsInAnyOrder("CUPON-A", "CUPON-B"));
    }

}
