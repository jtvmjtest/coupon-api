package net.jertocvil.couponapi.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import net.jertocvil.couponapi.controller.dto.PriceRequest;
import net.jertocvil.couponapi.controller.dto.Product;
import net.jertocvil.couponapi.exception.CouponDisabledException;
import net.jertocvil.couponapi.model.Coupon;
import net.jertocvil.couponapi.model.CouponType;
import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.service.impl.PriceServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceTest {

    Logger LOG = LoggerFactory.getLogger(PriceServiceTest.class);

    private HashSet<Product> products = new HashSet<>();

    public PriceServiceTest() {
        products.add(new Product("CAMISA", 3));
        products.add(new Product("TRAJE", 12));


    }

    @Mock
    private CouponRepository couponRepository;

    @InjectMocks
    private PriceServiceImpl priceService;

    @Test
    @Before
    public void contextLoads() {
        CouponType freeCouponType = new CouponType("GRATIS", "El descuento es igual al total", null, 100.0);
        CouponType amount10couponType = new CouponType("FIJO-10", "Descuenta 10 euros al total", 10.0, null);
        CouponType percent5couponType = new CouponType("PORCENTAJE-5", "Descuenta un 5% al total", null, 5.0);

        when(couponRepository.findByCouponCode("CUPON-A")).thenReturn(Optional.of(new Coupon("CUPON-A", freeCouponType, true)));
        when(couponRepository.findByCouponCode("CUPON-G")).thenReturn(Optional.of(new Coupon("CUPON-G", freeCouponType, false)));
        when(couponRepository.findByCouponCode("CUPON-D")).thenReturn(Optional.of(new Coupon("CUPON-D", amount10couponType, true)));
        when(couponRepository.findByCouponCode("CUPON-E")).thenReturn(Optional.of(new Coupon("CUPON-E", percent5couponType, true)));
    }

    @Test
    public void withoutCoupon() {
        this.priceAssert("NOEXISTE", 15.0);
    }


    @Test(expected = CouponDisabledException.class)
    public void inactiveCoupon() {
        PriceRequest request = new PriceRequest("CUPON-G", products);
        priceService.applyDiscount(request);
    }

    @Test
    public void freeCoupon() {
        this.priceAssert("CUPON-A", 0.0);
    }

    @Test
    public void withAmountCoupon() {
        this.priceAssert("CUPON-D", 5.0);
    }

    @Test
    public void withPercentageCoupon() {
        this.priceAssert("CUPON-E", 14.25);
    }

    @Test
    public void withCouponAmountGreaterThanPrice() {
        HashSet<Product> products = new HashSet<>();
        products.add(new Product("CAMISA", 3));
        PriceRequest request = new PriceRequest("CUPON-D", products);
        var response = priceService.applyDiscount(request);
        assertEquals(0.0, response.getDiscountedPrice(), 0.01);

    }


    @Test
    public void empty() {
        var coupons = priceService.searchCoupon("active", "NOEXISTE").getCoupons();
        assertEquals(0, coupons.size());

    }

    private void priceAssert(String coupon, double price) {
        PriceRequest request = new PriceRequest(coupon, products);
        var response = priceService.applyDiscount(request);
        assertEquals(price, response.getDiscountedPrice(), 0.01);

    }


}
