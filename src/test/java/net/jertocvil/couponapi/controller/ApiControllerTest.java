package net.jertocvil.couponapi.controller;


import net.jertocvil.couponapi.controller.dto.PriceRequest;
import net.jertocvil.couponapi.controller.dto.PriceResponse;
import net.jertocvil.couponapi.controller.dto.Product;
import net.jertocvil.couponapi.controller.dto.SearchCouponResponse;
import net.jertocvil.couponapi.model.CouponInfoProjection;
import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.repository.CouponTypeRepository;
import net.jertocvil.couponapi.service.PriceService;
import net.jertocvil.couponapi.service.impl.PriceServiceImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ApiController.class)
public class ApiControllerTest {

    @MockBean
    private PriceService priceService;

    @MockBean
    private CouponTypeRepository couponTypeRepository;

    @MockBean
    private CouponRepository couponRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void discount() throws Exception {
        var products = Set.of(new Product("CAMISA", 10.0), new Product("PANTALON", 20.0));
        var priceRequest = new PriceRequest("CUPON-A", products);
        var priceResponse = new PriceResponse(30.0, 0.0, products);

//        when(priceServiceImpl.applyDiscount(priceRequest), priceResponse);

        when(priceService.applyDiscount(priceRequest)).thenReturn(priceResponse);

        JSONObject camisa = new JSONObject();
        camisa.put("productCode", "CAMISA");
        camisa.put("price", 10.0);

        JSONObject pantalon = new JSONObject();
        pantalon.put("productCode", "PANTALON");
        pantalon.put("price", 20.0);


        JSONObject requestData = new JSONObject();
        requestData.put("couponCode", "CUPON-A");
        requestData.put("products", new JSONArray(Arrays.asList(camisa, pantalon)));

        mockMvc.perform(get("/coupon/discount").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8").content(requestData.toString())).andExpect(status().isOk())
                .andExpect(jsonPath("$.discountedPrice").value(is(0.0), Double.class));




    }

    @Test
    public void searchCoupon() throws Exception {

        CouponInfoProjection coupon1 = CouponInfoProjection.build("CODIGO1", "GRATIS",
                "Cupon mock 1", 100.0, null);
        CouponInfoProjection coupon2 = CouponInfoProjection.build("CODIGO2", "GRATIS",
                "Cupon mock 2", 100.0, null);

        when(priceService.searchCoupon(PriceService.STATUS_ACTIVE, "GRATIS")).thenReturn(new SearchCouponResponse(Arrays.asList(coupon1, coupon2)));
        when(priceService.searchCoupon(PriceService.STATUS_INACTIVE, "INEXISTENTE")).thenReturn(new SearchCouponResponse(Collections.emptyList()));

        mockMvc.perform(get("/coupon/search/{status}/{couponType}",
                PriceService.STATUS_INACTIVE, "INEXISTENTE").characterEncoding("utf-8"))
                .andExpect(status().isOk()).andExpect(jsonPath("$.coupons").isEmpty());

        mockMvc.perform(get("/coupon/search/{status}/{couponType}",
                PriceService.STATUS_ACTIVE, "GRATIS").characterEncoding("utf-8"))
                .andExpect(status().isOk()).andExpect(jsonPath("$.coupons[*].couponCode",
                containsInAnyOrder("CODIGO1", "CODIGO2")));
    }
}