package net.jertocvil.couponapi.model;

import javax.persistence.*;

@Entity
public class Coupon {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(length = 20, nullable = false, unique = true)
    private String couponCode;

    @ManyToOne
    private CouponType couponType;

    @Column(nullable = false)
    private boolean active;

    public Coupon() {

    }

    public Coupon(String couponCode, CouponType couponType, boolean active) {
        this.couponCode = couponCode;
        this.couponType = couponType;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public CouponType getCouponType() {
        return couponType;
    }

    public void setCouponType(CouponType couponType) {
        this.couponType = couponType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
