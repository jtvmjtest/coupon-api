package net.jertocvil.couponapi.model;


import javax.persistence.*;
import java.util.Set;

@Entity
public class CouponType {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(length = 20, nullable = false, unique = true)
    private String couponType;

    @Column(length = 200, nullable = true, unique = false)
    private String description;

    @Column(nullable = true)
    private Double discountAmount;

    @Column(nullable = true)
    private Double discountPercentage;

    @OneToMany(mappedBy = "couponType", fetch = FetchType.EAGER)
    private Set<Coupon> coupons;


    public CouponType() {

    }

    public CouponType(String couponType, String description, Double discountAmount, Double discountPercentage) {
        this.couponType = couponType;
        this.description = description;
        this.discountAmount = discountAmount;
        this.discountPercentage = discountPercentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Coupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(Set<Coupon> coupons) {
        this.coupons = coupons;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }
}
