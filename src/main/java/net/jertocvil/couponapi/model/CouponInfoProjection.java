package net.jertocvil.couponapi.model;

public interface CouponInfoProjection {

    public static CouponInfoProjection build(String couponCode, String couponType, String description, Double discountAmount, Double discountPercentage) {
        return new CouponInfoProjection() {

            @Override
            public String getCouponCode() {
                return couponCode;
            }

            @Override
            public CouponTypeInfoProjection getCouponType() {
                return new CouponTypeInfoProjection() {
                    @Override
                    public String getCouponType() {
                        return couponType;
                    }

                    @Override
                    public String getDescription() {
                        return description;
                    }

                    @Override
                    public Double getDiscountAmount() {
                        return discountAmount;
                    }

                    @Override
                    public Double getDiscountPercentage() {
                        return discountPercentage;
                    }
                };
            }
        };
    }

    String getCouponCode();
    CouponTypeInfoProjection getCouponType();


    public static interface CouponTypeInfoProjection {

        String getCouponType();
        String getDescription();
        Double getDiscountAmount();
        Double getDiscountPercentage();

    }

}
