package net.jertocvil.couponapi.service;

import net.jertocvil.couponapi.controller.dto.PriceRequest;
import net.jertocvil.couponapi.controller.dto.PriceResponse;
import net.jertocvil.couponapi.controller.dto.SearchCouponResponse;

public interface PriceService {

    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_INACTIVE = "inactive";

    PriceResponse applyDiscount(PriceRequest request);

    SearchCouponResponse searchCoupon(String status, String couponType);

}
