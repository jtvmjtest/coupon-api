package net.jertocvil.couponapi.service.impl;

import net.jertocvil.couponapi.controller.dto.PriceRequest;
import net.jertocvil.couponapi.controller.dto.PriceResponse;
import net.jertocvil.couponapi.controller.dto.Product;
import net.jertocvil.couponapi.controller.dto.SearchCouponResponse;
import net.jertocvil.couponapi.exception.CouponDisabledException;
import net.jertocvil.couponapi.exception.InvalidSearchRequestException;
import net.jertocvil.couponapi.model.CouponInfoProjection;
import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceServiceImpl implements PriceService {

    @Autowired
    private CouponRepository couponRepository;

    public PriceResponse applyDiscount(PriceRequest request) {
        var totalPrice = request.getProducts().stream()
                .map(Product::getPrice).reduce(0.0, Double::sum);

        var discountedPrice = totalPrice;

        var couponOpt = couponRepository.findByCouponCode(request.getCouponCode());

        if(couponOpt.isPresent()) {

            var coupon = couponOpt.get();

            if(!coupon.isActive()) {
                throw new CouponDisabledException("Coupon disabled");
            }

            if (coupon.getCouponType().getDiscountAmount() != null) {
                discountedPrice -= coupon.getCouponType().getDiscountAmount();
            } else if (coupon.getCouponType().getDiscountPercentage() != null) {
                discountedPrice *= (100.0 - coupon.getCouponType().getDiscountPercentage())/100.0;
            }

            if(discountedPrice < 0.0) {
                discountedPrice = 0.0;
            }
        }

        return new PriceResponse(totalPrice, discountedPrice, request.getProducts());

    }

    public SearchCouponResponse searchCoupon(String status, String couponType) {

        boolean active;

        if(status.equals(STATUS_ACTIVE)) {
            active = true;
        } else if(status.equals(STATUS_INACTIVE)) {
            active = false;
        } else {
            throw new InvalidSearchRequestException("Invalid search request");
        }

        List<CouponInfoProjection> coupons = couponRepository.findCouponsByCouponType_CouponTypeAndActive(couponType, active);

        return new SearchCouponResponse(coupons);

    }

}
