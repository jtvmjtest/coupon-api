package net.jertocvil.couponapi.controller.dto;

import java.util.Objects;
import java.util.Set;

public class PriceRequest {

    private String couponCode;
    private Set<Product> products;

    public PriceRequest() {
    }

    public PriceRequest(String couponCode, Set<Product> products) {
        this.couponCode = couponCode;
        this.products = products;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "PriceRequest{" +
                "couponCode='" + couponCode + '\'' +
                ", products=" + products +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PriceRequest)) return false;
        PriceRequest that = (PriceRequest) o;
        return Objects.equals(getCouponCode(), that.getCouponCode()) &&
                getProducts().equals(that.getProducts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCouponCode(), getProducts());
    }
}
