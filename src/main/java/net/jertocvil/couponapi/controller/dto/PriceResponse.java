package net.jertocvil.couponapi.controller.dto;

import java.util.Set;

public class PriceResponse {

    private double totalPrice, discountedPrice;
    private Set<Product> products;

    public PriceResponse(double totalPrice, double discountedPrice, Set<Product> products) {
        this.totalPrice = totalPrice;
        this.discountedPrice = discountedPrice;
        this.products = products;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
