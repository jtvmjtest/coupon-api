package net.jertocvil.couponapi.controller.dto;

import java.util.Objects;

public class Product {
    private String productCode;
    private double price;

    public Product() {

    }

    public Product(String productCode, double price) {
        this.productCode = productCode;
        this.price = price;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Double.compare(product.getPrice(), getPrice()) == 0 &&
                getProductCode().equals(product.getProductCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductCode(), getPrice());
    }
}