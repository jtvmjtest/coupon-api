package net.jertocvil.couponapi.controller.dto;

import net.jertocvil.couponapi.model.CouponInfoProjection;

import java.util.List;

public class SearchCouponResponse {

    private List<CouponInfoProjection> coupons;

    public SearchCouponResponse() {

    }

    public SearchCouponResponse(List<CouponInfoProjection> coupons) {
        this.coupons = coupons;
    }

    public List<CouponInfoProjection> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponInfoProjection> coupons) {
        this.coupons = coupons;
    }
}
