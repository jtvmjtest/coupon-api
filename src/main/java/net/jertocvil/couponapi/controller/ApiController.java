package net.jertocvil.couponapi.controller;

import net.jertocvil.couponapi.controller.dto.PriceRequest;
import net.jertocvil.couponapi.controller.dto.PriceResponse;
import net.jertocvil.couponapi.controller.dto.SearchCouponResponse;
import net.jertocvil.couponapi.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/coupon")
public class ApiController {

    @Autowired
    private PriceService priceService;

    @GetMapping("/discount")
    public PriceResponse discount(@RequestBody PriceRequest priceRequest) {
        return priceService.applyDiscount(priceRequest);
    }

    @GetMapping("/search/{status}/{couponType}")
    public SearchCouponResponse searchCoupon(@PathVariable String status, @PathVariable String couponType) {
        return priceService.searchCoupon(status, couponType);
    }

}
