package net.jertocvil.couponapi.repository;

import net.jertocvil.couponapi.model.Coupon;
import net.jertocvil.couponapi.model.CouponInfoProjection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CouponRepository extends JpaRepository<Coupon, Integer> {

    Optional<Coupon> findByCouponCode(String couponCode);

    List<CouponInfoProjection> findCouponsByCouponType_CouponTypeAndActive(String couponType, boolean active);
}
