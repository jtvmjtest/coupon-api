package net.jertocvil.couponapi.repository;

import net.jertocvil.couponapi.model.CouponType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponTypeRepository extends JpaRepository<CouponType, Integer> {
}
