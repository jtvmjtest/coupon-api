package net.jertocvil.couponapi.utils;

import net.jertocvil.couponapi.model.Coupon;
import net.jertocvil.couponapi.model.CouponType;
import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.repository.CouponTypeRepository;

import java.util.Arrays;

public class Utils {

    public static void dataInitialization(CouponRepository couponRepository, CouponTypeRepository couponTypeRepository) {

        CouponType freeCouponType = new CouponType("GRATIS", "El descuento es igual al total", null, 100.0);
        CouponType amount5couponType = new CouponType("FIJO-5", "Descuenta 5 euros al total", 5.0, null);
        CouponType amount10couponType = new CouponType("FIJO-10", "Descuenta 10 euros al total", 10.0, null);
        CouponType percent5couponType = new CouponType("PORCENTAJE-5", "Descuenta un 5% al total", null, 5.0);
        CouponType percent10couponType = new CouponType("PORCENTAJE-10", "Descuenta un 10% al total", null, 10.0);
        couponTypeRepository.saveAll(Arrays.asList(freeCouponType, amount5couponType, amount10couponType, percent5couponType, percent10couponType));

        Coupon couponA = new Coupon("CUPON-A", freeCouponType, true);
        Coupon couponB = new Coupon("CUPON-B", freeCouponType, true);
        Coupon couponC = new Coupon("CUPON-C", amount5couponType, true);
        Coupon couponD = new Coupon("CUPON-D", amount10couponType, true);
        Coupon couponE = new Coupon("CUPON-E", percent5couponType, true);
        Coupon couponF = new Coupon("CUPON-F", percent10couponType, true);
        Coupon couponG = new Coupon("CUPON-G", freeCouponType, false);
        couponRepository.saveAll(Arrays.asList(couponA, couponB, couponC, couponD, couponE, couponF, couponG));

    }

}
