package net.jertocvil.couponapi;

import net.jertocvil.couponapi.model.Coupon;
import net.jertocvil.couponapi.model.CouponType;
import net.jertocvil.couponapi.repository.CouponRepository;
import net.jertocvil.couponapi.repository.CouponTypeRepository;
import net.jertocvil.couponapi.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class CouponApiApplication implements CommandLineRunner {

    Logger LOG = LoggerFactory.getLogger(CouponApiApplication.class);

    @Autowired
    private CouponTypeRepository couponTypeRepository;

    @Autowired
    private CouponRepository couponRepository;

    public static void main(String[] args) {
        SpringApplication.run(CouponApiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if(couponRepository.count() == 0 && couponTypeRepository.count() == 0) {
            LOG.info("Initializing database data");
            Utils.dataInitialization(couponRepository, couponTypeRepository);
        }

    }

}
